info_print "Enabling Reflector, automatic snapshots, BTRFS scrubbing and systemd-oomd."
set services reflector.timer snapper-timeline.timer snapper-cleanup.timer btrfs-scrub@-.timer btrfs-scrub@home.timer btrfs-scrub@var-log.timer btrfs-scrub@\\x2esnapshots.timer grub-btrfs.path systemd-oomd
for service in services; do
    systemctl enable "$(service)" --root=/
done
